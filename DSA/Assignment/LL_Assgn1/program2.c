#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct States {

	char name[20];
	double pop;
	float bud;
	float lit;
	struct States* next;
}st;

st* head = NULL;

void addNode() {

	st* newNode = (st*)malloc(sizeof(st));
	printf("Enter the state name\n");
	fgets(newNode->name,15,stdin);
	printf("Enter population\n");
	scanf("%lf",&newNode->pop);
	printf("Budget\n");
	scanf("%f",&newNode->bud);
	printf("Literacy\n");
	scanf("%f",&newNode->lit);
	getchar();

	newNode->next = NULL;

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		st* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL() {

	st* temp = head;
	while(temp != NULL) {

		int size = strlen(temp->name);
		temp->name[size-1] = '\0';
	
		printf("| %s -> ",temp->name);
		printf("%lf -> ",temp->pop);
		printf("%f -> ",temp->bud);
		printf("%f |",temp->lit);
		temp = temp->next;
	}
}

void main() {

	addNode();
	addNode();
	addNode();
	printLL();
}
