#include<stdio.h>
#include<stdlib.h>

typedef struct PlayList{

	char sName[20];
	float duration;
	struct PlayList *next;
}pl;

pl* head = NULL;

void addNode() {

	pl *newNode = (pl*)malloc(sizeof(pl));
	printf("Enter song name\n");
	int i=0;
	char ch;
	getchar();
	while((ch = getchar()) != '\n') {
	
		(*newNode).sName[i] = ch;
		i++;
	}
	printf("Enter song duration\n");
	scanf("%f",&newNode->duration);

	newNode->next = NULL;

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		pl* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL() {

	pl* temp = head;
	while(temp != NULL) {
	
		printf("| %s->",temp->sName);
		printf("%f |",temp->duration);
		temp = temp->next;
	}
}

void main() {

	int x;
	printf("Enter number of nodes\n");
	scanf("%d",&x);
	for(int i=0; i<x; i++) {
	
		addNode();
	}
	printLL();
}










