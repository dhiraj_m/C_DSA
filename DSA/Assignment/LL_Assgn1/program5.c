#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Playlist{

	char name[20];
	float duration;
	struct Demo* next;
}dm;

dm* head = NULL;

void addNode() {

	dm* newNode = (dm*)malloc(sizeof(dm));
	
	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	getchar();

	newNode->next = NULL;

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		dm* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

int sumNode() {

	dm* temp = head;
	int sum = 0;
	while(temp != NULL) {

		sum = sum + temp->data;
		temp = temp->next;
	}
	return sum;
}

int addFirstandLast() {

	dm* temp = head;
	int sumFL = temp->data;
	while(temp->next != NULL) {
	
		temp = temp->next;
	}
	sumFL = sumFL + temp->data;
	return sumFL;
}

int maxData() {

	dm* temp = head;
	int max = 0;
	while(temp != NULL) {
	
		if(temp->data > max) {
		
			max = temp->data;
		}
		temp = temp->next;
	}
	return max;
}

int minData() {

	dm* temp = head;
	int min = temp->data;
	while(temp != NULL) {
	
		if(temp->data < min) {
		
			min = temp->data;
		}
		temp = temp->next;
	}
	return min;
}

void main() {

	int x;
	printf("Enter number of nodes you want\n");
	scanf("%d",&x);

	for(int i=0; i<x; i++) {
	
		addNode();
	}
	printf("Sum of data in all nodes is %d\n",sumNode());
	printf("Sum of first and last data in nodes is %d\n",addFirstandLast());
	printf("Max data in nodes is %d\n",maxData());
	printf("Min data in nodes is %d\n",minData());
}
