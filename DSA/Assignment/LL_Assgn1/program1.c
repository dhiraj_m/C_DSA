#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Mall {

	char name[20];
	int nShops;
	float rev;
	struct Mall* next;
}M;

M* head = NULL;

void addNode() {

	M* newNode = (M*)malloc(sizeof(M));
	printf("Enter the mall name\n");
	fgets(newNode->name,15,stdin);
	printf("Enter no. of shops\n");
	scanf("%d",&newNode->nShops);
	printf("Revenue\n");
	scanf("%f",&newNode->rev);
	getchar();

	newNode->next = NULL;

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		M* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL() {

	M* temp = head;
	while(temp != NULL) {

		int size = strlen(temp->name);
		temp->name[size-1] = '\0';
	
		printf("| %s -> ",temp->name);
		printf("%d -> ",temp->nShops);
		printf("%f |",temp->rev);
		temp = temp->next;
	}
	printf("\n");
}

void main() {

	addNode();
	addNode();
	addNode();
	printLL();
}
