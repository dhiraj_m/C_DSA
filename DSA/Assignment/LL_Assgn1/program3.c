#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Festival{

	char name[20];
	int days;
	struct Festival* next;
}Fest;

Fest* head = NULL;
int count = 0;

void addNode() {

	Fest* newNode = (Fest*)malloc(sizeof(Fest));
	printf("Enter the fest name\n");
	fgets(newNode->name,15,stdin);
	printf("Enter no. of days\n");
	scanf("%d",&newNode->days);

	getchar();

	newNode->next = NULL;

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		Fest* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
	count++;
}

void printLL() {

	Fest* temp = head;
	while(temp != NULL) {

		int size = strlen(temp->name);
		temp->name[size-1] = '\0';
	
		printf("| %s -> ",temp->name);
		printf("%d |",temp->days);
		temp = temp->next;
	}
}

void main() {

	addNode();
	addNode();
	addNode();
	printf("\nNumber of nodes are %d\n\n",count);
	printLL();
}
