#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

Node* head = NULL;

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		Node* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void countOccurrence(int data) {

	if(head == NULL) {
	
		printf("No node found");
	}else {
		
		Node* temp = head;
		int count = 0;
		while(temp != NULL) {
	
			if(temp->data == data) {
		
				count++;
			}
			temp = temp->next;
		}
		if(count == 0) {
	
			printf("No data found\n");
		}else {
	
			printf("Count of your data is %d\n",count);
		}
	}
}
void main() {

	int nodes,data;
	char choice;
	do { 
	
		printf("Enter valid number of nodes\n");
		scanf("%d",&nodes);
	} while(nodes <= 0);

	for(int i=0; i<nodes; i++) {
	
		addNode();
	}
	do {
	
		printf("Enter data to find\n");
		scanf("%d",&data);
		countOccurrence(data);
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	} while(choice == 'Y' || choice == 'y');
}
