#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

Node* head[2] = {NULL};

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));

	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();

	if(head[0] == NULL) {
	
		head[0] = newNode;
	}else {
	
		Node* temp = head[0];
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void addRange(int start,int end) {

	int r = end-start+1;
	Node* temp1 = head[0];
	while(start-1) {
	
		temp1 = temp1->next;
		start--;
	}
	while(r && temp1 != NULL) {
	
		Node* newNode = (Node*)malloc(sizeof(Node));
		newNode->data = temp1->data;
		newNode->next = NULL;

		if(head[1] == NULL) 
			head[1] = newNode;
		else {
		
			Node* temp2 = head[1];
			while(temp2->next != NULL) {
			
				temp2 = temp2->next;
			}
			temp2->next = newNode;
		}
		r--;
		temp1 = temp1->next;
	}
}
void printLL() {

	for(int i=0; i<2; i++) {
	
		if(head[i] == NULL) {
		
			printf("%dst linkedlist Empty\n",i+1);
		} else {

			printf("%dst linked list is\n",i+1);
			Node* temp = head[i];
			while(temp->next != NULL) {
		
				printf("|%d|->",temp->data);
				temp = temp->next;
			}
			printf("|%d|\n",temp->data);
		}
	}
}
void main() {

	int node,start,end;

	do {

		printf("Enter valid numbers of node\n");
		scanf("%d",&node);	
	} while(node<=0);

	for(int i=0; i<node; i++)
		addNode();

	printf("Enter range between %d and %d\n",1,node);
	do {
	
		printf("Enter valid start\n");
		scanf("%d",&start);
	} while(start<1 || start>node);
	do {
	
		printf("Enter valid end\n");
		scanf("%d",&end);
	} while(end<start);
	addRange(start,end);
	printLL();
}
