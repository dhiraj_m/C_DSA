#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

Node* head = NULL;
Node* rear = NULL;

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {

		rear = head;
		while(rear->next != NULL) {
		
			rear = rear->next;
		}
		if(head->data > newNode->data) {
	
			newNode->next = head;
			head = newNode;	
		} else if(rear->data <= newNode->data) {
		
			rear->next = newNode;
			rear = rear->next;
		} else {
		
			Node* temp = head;
			while(temp->next->data <= newNode->data) {
			
				temp = temp->next;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
	}
}
void printLL() {
	
	if(head == NULL) {
		
		printf("linkedlist Empty\n");
	} else {

		printf("linked list is\n");
		Node* temp = head;
		while(temp->next != NULL) {
		
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
	
}
void main() {

	int nodes,data;
	char choice;
	do { 
	
		printf("Enter valid number of nodes\n");
		scanf("%d",&nodes);
	} while(nodes <= 0);

	for(int i=0; i<nodes; i++) {
	
		addNode();
	}
	printLL();
}
