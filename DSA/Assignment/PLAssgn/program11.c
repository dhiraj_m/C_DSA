#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

int i=0;
int num1 = 0;

Node* head[2] ={NULL};

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	if(i == 0) {
	
		printf("Enter data\n");
		scanf("%d",&newNode->data);
	} else
		newNode->data = num1;

	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();

	if(head[i] == NULL) {
	
		head[i] = newNode;
	}else {
	
		Node* temp = head[i];
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void isPrime() {

	i++;
	Node* temp = head[0];
	while(temp != NULL) {

		if(temp->data != 0 && temp->data != 1) {

			int count = 0;
			for(int j=2; j<=sqrt(temp->data); j++) {
		
				if(temp->data%j == 0 && temp->data!=j) {
			
					count++;
					break;
				}
			}
			if(count == 0) {
		
				num1 = temp->data;
				addNode();
			}
		}
		temp = temp->next;
	}
}
void printLL() {

	for(int i=0; i<2; i++) {
	
		if(head[i] == NULL) {
		
			printf("%dst linkedlist Empty\n",i+1);
		} else {

			printf("%dst linked list is\n",i+1);
			Node* temp = head[i];
			while(temp->next != NULL) {
		
				printf("|%d|->",temp->data);
				temp = temp->next;
			}
			printf("|%d|\n",temp->data);
		}
	}
}
void main() {

	int node;

	do {

		printf("Enter number of nodes\n");
		scanf("%d",&node);
	} while(node < 0);

	for(int i=0; i<node; i++) {
	
		addNode();
	}
	isPrime();
	printLL();
}
