#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;
int i = 0;
Node* head[2] ={NULL};

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();

	if(head[i] == NULL) {
	
		head[i] = newNode;
	}else {
	
		Node* temp = head[i];
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void printLL() {

	for(int i=0; i<2; i++) {
	
		if(head[i] == NULL) {
		
			printf("%dst linkedlist Empty\n",i+1);
		} else {

			printf("%dst linked list is\n",i+1);
			Node* temp = head[i];
			while(temp->next != NULL) {
		
				printf("|%d|->",temp->data);
				temp = temp->next;
			}
			printf("|%d|\n",temp->data);
		}
	}
}
int countNode() {

	int count = 0;
	Node* temp = head[0];
	while(temp != NULL) {
	
		temp = temp->next;
		count++;
	}
	return count;
}
int subSetAt() {

	if(head[0] == 0 || head[1] == 0) {
	
		printf("One or both linkedList are empty\n");
		return -1;
	} else {

		int a = 0;
		int b = 1;
		Node* temp1 = head[0];
		Node* temp2 = head[1];
		while(temp2 != NULL && temp1 != NULL) {
		
			if(temp1->data == temp2->data) {
		
				if(a == 0) 
					a = b;
				temp1 = temp1->next;
			}
			temp2 = temp2->next;
			b++;
		}
		int count = countNode();
		//printf("%d\n",count);
		//printf("%d\n",a);
		//printf("%d\n",b);
		if(b-a == count) {
		
			return a;
		} else {
		
			printf("No subset found\n");
			return 0;
		}
	}
}
void main() {

	int node1,node2;
	do {

		printf("Enter valid size nodes of list1\n");
		scanf("%d",&node1);
	} while(node1<0);
	for(int i=0; i<node1; i++) {
	
		addNode();
	}
	i++;
	do {
	
		printf("Enter valid size nodes of list2\n");
		scanf("%d",&node2);
	} while(node2<0);
	for(int i=0; i<node2; i++) {
	
		addNode();
	}
	printLL();
	int s = subSetAt();
	printf("%d\n",s);
}
