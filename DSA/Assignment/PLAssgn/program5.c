#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

int i=0;
Node* head[2] ={NULL};

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();

	if(head[i] == NULL) {
	
		head[i] = newNode;
	}else {
	
		Node* temp = head[i];
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
int countNode() {

	int count = 0;
	if(head[0] == NULL) {
	
		return 0;
	} else {
	
		Node* temp = head[0];
		while(temp != NULL) {
		
			count++;
			temp = temp->next;
		}
		return count;
	}
}
int concatRangeelements(int n1,int n2) {

	int b = 1;
	int count = countNode();
	if(head[0] == NULL) {

		printf("Source LL is empty\n");
		return -1;
	} else if(n1>count) {
	
		printf("Invalid position\n");
		return -1;
	}else {

	//	Node* temp1 = head[1];
		//Node* temp2 = head[0];
		/*if(n1-n2 == 0) {

			printf("No change in LinkedList ");
		} else {*/

			int c = n1;
			Node* temp1 = head[1];
			Node* temp2 = head[0];
			while(n1-1 && temp2->next != NULL && b) {
			
				temp2 = temp2->next;
				n1--;
			}
			if(head[1] == NULL) {
			
				head[1] = temp2;
				temp1 = head[1];
				b = 0;
			}
			while(temp1->next != NULL && b) {
			
				temp1 = temp1->next;
			}
			if(b) {

				temp1->next = temp2;
				temp1 = temp1->next;
			}
			while(n2-c && temp1->next != NULL) {
			
				temp1 = temp1->next;
				c++;
			}
			temp1->next = NULL;
		//}
		printf("After concatination\n");
		temp1 = head[1];
		while(temp1->next != NULL) {
			
			printf("|%d|->",temp1->data);
			temp1 = temp1->next;
		}
		printf("|%d|\n",temp1->data);
	}
}
void printLL() {

	for(int i=0; i<2; i++) {
	
		if(head[i] == NULL) {
		
			printf("%dst linkedlist Empty\n",i+1);
		} else {

			printf("%dst linked list is\n",i+1);
			Node* temp = head[i];
			while(temp->next != NULL) {
		
				printf("|%d|->",temp->data);
				temp = temp->next;
			}
			printf("|%d|\n",temp->data);
		}
	}
}
void main() {

	int node1,node2,n1,n2;
	do {

		printf("Enter valid size nodes of list1\n");
		scanf("%d",&node1);
	} while(node1<0);
	for(int i=0; i<node1; i++) {
	
		addNode();
	}
	i++;
	do {
	
		printf("Enter valid size nodes of list2\n");
		scanf("%d",&node2);
	} while(node2<0);
	for(int i=0; i<node2; i++) {
	
		addNode();
	}
	do {

		printf("Enter valid number of positions to add in destination list\n");
		scanf("%d",&n1);
		scanf("%d",&n2);
	} while(n1<0 || n1>n2);
	printLL();
	concatRangeelements(n1,n2);
}
