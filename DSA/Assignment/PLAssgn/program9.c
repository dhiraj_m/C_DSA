#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

int i=0;

Node* head[2] ={NULL};

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));

	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();

	if(head[0] == NULL) {
	
		head[0] = newNode;
	}else {
	
		Node* temp = head[0];
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void addAlter() {

	int num = 1;
	Node* temp = head[0];
	while(temp != NULL) {
	
		if(num%2 == 1) {
	
			Node* newNode = (Node*)malloc(sizeof(Node));
			newNode->data = temp->data;
			newNode->next = NULL;

			if(head[1] == NULL) {
	
				head[1] = newNode;
			} else {
	
				Node* temp2 = head[1];
				while(temp2->next != NULL) {
		
					temp2 = temp2->next;
				}
				temp2->next = newNode;
			}
		}
		temp = temp->next;
		num++;
	}
}
void printLL() {

	for(int i=0; i<2; i++) {
	
		if(head[i] == NULL) {
		
			printf("%dst linkedlist Empty\n",i+1);
		} else {

			printf("%dst linked list is\n",i+1);
			Node* temp = head[i];
			while(temp->next != NULL) {
		
				printf("|%d|->",temp->data);
				temp = temp->next;
			}
			printf("|%d|\n",temp->data);
		}
	}
}
void main() {

	int node;

	do {

		printf("Enter number of nodes\n");
		scanf("%d",&node);
	} while(node < 0);

	for(int i=0; i<node; i++) {
	
		addNode();
	}
	addAlter();
	printLL();
}
