#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

Node* head = NULL;

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		Node* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void firstOccurrence(int data) {

	Node* temp = head;
	int pos = 0;
	if(head == NULL) {
	
		printf("No node present\n");
	}else {
	
		while(temp != NULL) {
	
			pos++;
			if(temp->data == data) {
		
				temp = temp->next;
			}
		}
	}
	if(pos == 0) {
	
		printf("No data found\n");
	}else {
	
		printf("First occurrrence of your data is at %d\n",pos);
	}	
}
void secondLastOccurrence(int data) {

	if(head == NULL) {
	
		printf("No Node present\n");
	}else {
	
		Node* temp = head;
		int p1 = 0, p2 = 0, count = 1;
		while(temp != NULL) {
		
			if(temp->data == data) {
			
				p2 = p1;
				p1 = count;
				temp = temp->next;
			}
			count++;
		}
		if(p1 == 0) {
		
			printf("No element found\n");
		}else if(p2 == 0) {
		
			printf("Found only once\n");
		}else {
		
			printf("Position of second last data is %d\n",p2);
		}
	}
}
void countOccurrence(int data) {

	if(head == NULL) {
	
		printf("No node found");
	}else {
		
		Node* temp = head;
		int count = 0;
		while(temp != NULL) {
	
			if(temp->data == data) {
		
				count++;
			}
			temp = temp->next;
		}
		if(count == 0) {
	
			printf("No data found\n");
		}else {
	
			printf("Count of your data is %d\n",count);
		}
	}
}
void sumDigit() {

	if(head == NULL) {
	
		printf("No Node present\n");
	}else {
		
		Node* temp = head;
		while(temp != NULL) {
	
			int sum = 0;
			while(temp->data != 0) {
		
				sum = sum + temp->data%10;
				temp->data = temp->data/10;
			}
			temp->data = sum;
			temp = temp->next;
		}
	}
}
void palinDrome() {

	int count = 1;

	if(head == NULL) {
	
		printf("No node present\n");
	}else {
	
		Node* temp = head;
		while(temp != NULL) {
		
			int temp2 = temp->data;
			int pal = 0;
			while(temp2 != 0) {
			
				pal = pal*10 + temp->data%10;
				temp2 = temp2/10;
			}
			if(temp->data == pal) {
			
				printf("Palindrome found at %d",count);
			}
			count++;
			temp = temp->next;
		}
	}
}
void printLL() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else {
	
		Node* temp = head;
		while(temp->next != NULL) {
		
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
void main() {

	char ch;
	int data1;
	do {
	
		int a;
		printf("1.To add Node\n");
		printf("2.For First Occurrence\n");
		printf("3.For Second Last Occurrence\n");
		printf("4.For count of occurrrence\n");
		printf("5.Palindrome\n");
		printf("6.printLL\n");
		printf("Enter your choice\n");
		scanf("%d",&a);

		switch(a) {

			case 1:
				addNode();
				break;
		
			case 2:
				{
					printf("Enter data to find\n");
					scanf("%d",&data1);
					firstOccurrence(data1);
				}
				break;
			case 3:
			       {
				       int data2;
				       printf("Enter data to find\n");
				       scanf("%d",&data2);
				       secondLastOccurrence(data2);
			       }
			       break;
			case 4:
			       {
			       
				       int data3;
				       printf("Enter data to find\n");
				       scanf("%d",&data3);
				       countOccurrence(data3);
			       }
			       break;
			case 5:
			       palinDrome();
			       break;
			case 6:
			       printLL();
			       break;
			default:
			       printf("Invalid Input\n");
		}
		printf("Do you want to continue?\n");
		scanf(" %c",&ch);

	}while(ch == 'Y' || ch == 'y');
}
