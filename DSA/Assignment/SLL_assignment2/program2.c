#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Node {

	char str[20];
	struct Node* next;
}Node;

Node* head = NULL;

int countNode() {

	int count = 0;
	Node* temp = head;
	while(temp != NULL) {
	
		count++;
		temp = temp->next;
	}
	return count;
}


Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	
	int i=0;
	char ch;
	printf("Enter string\n");
	while((ch = getchar()) != '\n') {
	
		(*newNode).str[i] = ch;
		i++;
	}
	newNode->next = NULL;
	return newNode;
}
void addNode() {

	Node* newNode = createNode();
	if(head == NULL) {
	
		head = newNode;
	}else {
	
		Node* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void LengthStr(int n) {

	if(head == NULL) {
	
		printf("No node present\n");
	}else {
		
		Node* temp = head;
		while(temp != NULL) {
		
			int len = 0;
			char* ch = temp->str;
			while(*ch != '\0') {
			
				len++;
				ch++;
			}
			if(len == n) {
			
				printf("%s\n",temp->str);
			}
			temp = temp->next;
		}
	}
}
void reverseNode() {

	if(head == NULL) {
	
		printf("No Node present\n");
	}else {
	
		Node* temp = head;
		while(temp != NULL) {
		
			int n=0,i=0;
			char* ch = temp->str;
			char temp2[20];
			while(*ch != '\0') {

				ch++;
				n++;
			}
			ch--;
			while(n) {
			
				temp2[i] = *ch;
				ch--;
				n--;
				i++;
			}
			i = 0;
			n = strlen(temp->str);
			while(n) {
			
				temp->str[i] = temp2[i];
				i++;
				n--;
			}
			temp = temp->next;
		}
	}
	getchar();

}
void deleteFirst() {

	if(head == NULL) {
	
		printf("No node is present\n");
	}else {

		Node* temp = head;
		head = head->next;
		free(temp);
	}
}
void deleteLast() {

	if(head == NULL) {
	
		printf("No node is present\n");
	}else if(head->next == NULL) {
	
		deleteFirst();
	}else {

		Node* temp = head;
		while(temp->next->next != NULL) {
		
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}
void delAtPos(int pos) {

	int count = countNode();
	if(pos<=0 || pos>count) {
	
		printf("Invalid input\n");
	}else if(pos == 1) {
	
		deleteFirst();
	}else if(pos == count) {
	
		deleteLast();
	}else {
	
		Node* temp1 = head;
		Node* temp2 = head;
		while(pos-2) {
		
			temp1 = temp1->next;
			pos--;
		}
		while(pos-1) {
		
			temp2 = temp2->next;
			pos--;
		}
		temp1->next = temp2->next;
		free(temp2);
	}
}
void delLenNode(int n) {

	Node* temp = head;
	int count = 1;
	while(temp != NULL) {
	
		if(strlen(temp->str) != n) {
		
			delAtPos(count);
		}
		count++;
		temp = temp->next;
	}
}
void printLL() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else {
	
		Node* temp = head;
		while(temp->next != NULL) {
		
			printf("|%s|->",temp->str);
			temp = temp->next;
		}
		printf("|%s|\n",temp->str);
	}
	
}
void main() {

	char choice;
	do {
	
		printf("1.To add Node\n");
		printf("2.To print string in LL of your length\n");
		printf("3.To reverse node data\n");
		printf("4.delLenNode\n");
		printf("5.To printLL\n");

		int i;
		printf("Enter your choice\n");
		scanf("%d",&i);

		switch(i) {
		
			case 1:
				getchar();
				addNode();
				break;
			case 2:
				{
				
					int len;
					printf("Enter length of which you want string\n");
					scanf("%d",&len);
					LengthStr(len);
					getchar();
				}
				break;
			case 3:
				reverseNode();
				break;
			case 4:
				{
				
					int len;
					printf("Enter length of which you don't want delete\n");
					scanf("%d",&len);
					delLenNode(len);
					getchar();
				}
				break;
			case 5:
				printLL();
				getchar();
				break;
			default:
				printf("Invalid input\n");
		}
		printf("Do you want to continue\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

