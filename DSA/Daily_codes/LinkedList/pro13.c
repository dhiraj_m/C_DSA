#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	struct Node* prev;
	int data;
	struct Node* next;
}Node;

Node* head = NULL;

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->prev = NULL;
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
		newNode->prev = head;
		newNode->next = head;
	}else {
	
		Node* temp = head;
		while(temp->next != head) {
		
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
		newNode->next = head;
		head->prev = newNode;

	}
}
void addFirst() {

	Node* newNode = createNode();
	if(head == NULL) {
	
		head = newNode;
		newNode->next = head;
		newNode->prev = head;
	}else {
	
		Node* temp = head;
		while(temp->next != head) {
		
			temp = temp->next;
		}
		newNode->next = head;
		head->prev = newNode;
		temp->next = newNode;
		newNode->prev = temp;
		head = newNode;
	}
}
int countNode() {

	int count = 1;
	if(head == NULL) {
	
		return 0;
	}else {
	
		Node* temp = head;
		while(temp->next != head) {
		
			count++;
			temp = temp->next;
		}
		return count;
	}
}
void addAtPos(int pos) {

	int count = countNode();
	if(pos<=0 || pos>count+1) {
	
		printf("Invalid position\n");
	}else {
	
		if(pos == 1) {
		
			addFirst();
		}else if(pos == count+1) {
		
			addNode();
		}else {
		
			Node* temp = head;
			Node* newNode = createNode();
			while(pos-2) {
			
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			newNode->next->prev = newNode;
			temp->next = newNode;
			newNode->prev = temp;
		}
	}
}
void delFirst() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else if(head->next == head){
	
		free(head);
		head = NULL;
	}else {
	
		Node* temp = head;
		while(temp->next != head) {
		
			temp = temp->next;
		}
		head = head->next;
		free(temp->next);
		temp->next = head;
		head->prev = temp;
	}
}
void delLast() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else if(head->next == head){
	
		free(head);
		head = NULL;
	}else {
	
		Node* temp = head;
		while(temp->next->next != head) {
		
			temp = temp->next;
		}
		free(temp->next);
		temp->next = head;
		head->prev = temp;		
	}
}
void delAtPos(int pos) {

	int count = countNode();
	int p1 = pos;
	if(pos<=0 || pos>count) {
	
		printf("Enter valid position\n");
	}else {
	
		if(pos == 1) {
		
			delFirst();
		}else if(pos == count) {
		
			delLast();
		}else {
		
			Node* temp1 = head;
			Node* temp2 = head;
			while(pos-2) {
			
				temp1 = temp1->next;
				pos--;
			}
			while(p1-1) {
			
				temp2 = temp2->next;
				p1--;
			}
			temp1->next = temp2->next;
			temp2->next->prev = temp1;
			free(temp2);
		}
	}
}
void printDCLL() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else {

		Node* temp = head;
		while(temp->next != head) {
	
			printf("|%d|->",temp->data);
			temp = temp->next;	
		}
		printf("|%d|\n",temp->data);
	}
}
void main() {

	char choice;
	do{
	
		int i;
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.countNode\n");
		printf("6.delFirst\n");
		printf("7.delLast\n");
		printf("8.delAtPos\n");
		printf("9.printDCLL\n");
		printf("Enter your choice\n");
		scanf("%d",&i);

		switch(i) {
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addNode();
				break;
			case 4:
				{
					int pos;
					printf("Enter position at which you want to add node\n");
					scanf("%d",&pos);
					addAtPos(pos);
				}
				break;
			case 5:
				countNode();
				break;
			case 6:
				delFirst();
				break;
			case 7:
				delLast();
				break;
			case 8:
				{
					int pos;
					printf("Enter the position you want to delete\n");
					scanf("%d",&pos);
					delAtPos(pos);
				}
				break;
			case 9:
				printDCLL();
				break;
			default:
				printf("Invalid choice\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

