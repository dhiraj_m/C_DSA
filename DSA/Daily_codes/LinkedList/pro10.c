#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node* next;
}N1;

void main() {

	N1* head = NULL;
	N1* newNode = (N1*)malloc(sizeof(N1));

	newNode->data = 10;
	newNode->next = NULL;
	head = newNode;

	newNode = (N1*)malloc(sizeof(N1));
	newNode->data = 20;
	newNode->next = NULL;
	head->next = newNode;

	newNode = (N1*)malloc(sizeof(N1));
	newNode->data = 30;
	newNode->next = NULL;
	head->next->next = newNode;

	N1* temp = head;
	while(temp != NULL) {
	
		printf("%d\n",temp->data);
		temp = temp->next;
	}
}
