#include<stdio.h>
#include<stdlib.h>

typedef struct Theatre{

	struct Theatre* prev;
	int chairNo;
	struct Theatre* next;
}T;

T* head = NULL;
int countNode();
T* createNode() {

	T* newNode = (T*)malloc(sizeof(T));
	newNode->prev = NULL;
	getchar();
	printf("Enter chair num\n");
	scanf("%d",&newNode->chairNo);
	newNode->next = NULL;

	return newNode;
}

void addNode() {

	T* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		T* temp = head; 
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}

void addFirst() {

	T* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}
}
void addAtPos(int pos) {

	int count = countNode();
	if(pos<=0 || pos>=count+2) {
	
		printf("Wrong Position\n");
	}else {

		if(pos == 1) {
		
			addFirst();
		}else if(pos == count+1) {
		
			addNode();
		}else {

			T* newNode = createNode();
			T* temp = head;

			while(pos-2) {
	
				temp = temp->next;
			}
			newNode->next = temp->next;
			newNode->prev = temp;
			temp->next->prev = newNode;
			temp->next = newNode;
		}
	}
}
void deleteFirst() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else if(head->next == NULL) {
	
		free(head);
		head == NULL;
	}else {
	
		head = head->next;
		free(head->prev);
		head->prev = NULL;
	}
}
void deleteLast() {

	if(head == NULL) {
	
		printf("No Node present\n");
	}else if(head->next == NULL) {
	
		free(head);
		head = NULL;
	}else {
	
		T* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->prev->next = NULL;
		free(temp);
	}
}
void delAtPos(int pos) {

	int count = countNode();
	if(pos<=0 || pos>count) {
	
		printf("Wrong position\n");
	}else {
	
		if(pos == 1) {
		
			deleteFirst();
		}else if(pos == count) {
		
			deleteLast();
		}else {
		
			T* temp = head;
			while(pos-2) {
			
				temp = temp->next;
				pos--;
			}
			temp->next = temp->next->next;
			free(temp->next->prev);
			temp->next->prev = temp;
		}
	}
}
int countNode() {

	int count = 0;
	T* temp = head;
	while(temp != NULL) {
	
		count++;
		temp = temp->next;
	}
	return  count;
}
void printLL() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else {

		T* temp = head;
		while(temp->next != NULL) {
	
			printf("|%d|->",temp->chairNo);
			temp = temp->next;
		}
		printf("|%d|\n",temp->chairNo);
	}
}
void main() {

	char choice;
	do {

		int n;
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.countNode\n");
		printf("6.delFirst\n");
		printf("7.delLast\n");
		printf("8.delAtPos\n");
		printf("9.printLL\n");

		printf("Enter your choice\n");
		scanf("%d",&n);

		switch(n) {
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addNode();
				break;
			case 4:
				{
					int pos2;
					printf("Enter position to add node\n");
					scanf("%d",&pos2);
					addAtPos(pos2);
				}
				break;
			case 5:
				countNode();
				break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			case 8:
				{
					int pos;
					printf("Enter Position to delete node\n");
					scanf("%d",&pos);
					delAtPos(pos);
				}
				break;
			case 9:
				printLL();
				break;
			default:
				printf("Invalid Input\n");
		}
		getchar();
		printf("Do you want continue?\n");
		scanf(" %c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
