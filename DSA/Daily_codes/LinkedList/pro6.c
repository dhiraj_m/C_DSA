#include<stdio.h>
#include<string.h>

struct Batter {

	int jerNo;
	char name[20];
	float avg;
	struct Batter *next;
};
void main() {

	struct Batter obj1,obj2,obj3;
	struct Batter *head = &obj1;

	head->jerNo = 18;
	strcpy(head->name,"Virat");
	head->avg = 54.55;
	head->next = &obj2;
	
	head->next->jerNo = 45;
	strcpy(head->next->name,"Rohit");
	head->next->avg = 44.55;
	head->next->next = &obj3;
	
	head->next->next->jerNo = 7;
	strcpy(head->next->next->name,"MSD");
	head->next->next->avg = 50.55;
	head->next->next->next = NULL;
}
