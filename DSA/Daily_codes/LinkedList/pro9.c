#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Movie {

	int tCount;
	char mName[20];
	float imdb;
	struct Movie* next;
}Mov;

void accessData(Mov* ptr) {

	printf("%d\n",ptr->tCount);
	printf("%s\n",ptr->mName);
	printf("%f\n",ptr->imdb);
	printf("%p\n\n",ptr->next);
}

void main() {

	Mov* mv1 = (Mov*)malloc(sizeof(Mov));
	Mov* mv2 = (Mov*)malloc(sizeof(Mov));
	Mov* mv3 = (Mov*)malloc(sizeof(Mov));

	mv1->tCount = 3;
	strcpy(mv1->mName,"Kantara");
	mv1->imdb = 9.3;
	mv1->next = mv2;
	
	mv2->tCount = 5;
	strcpy(mv2->mName,"D2");
	mv2->imdb = 9.1;
	mv2->next = mv3;
	
	mv3->tCount = 7;
	strcpy(mv3->mName,"KGF2");
	mv3->imdb = 9.8;
	mv3->next = NULL;

	accessData(mv1);
	accessData(mv2);
	accessData(mv3);
}
