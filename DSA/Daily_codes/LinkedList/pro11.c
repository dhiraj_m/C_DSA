#include<stdio.h>
#include<stdlib.h>

typedef struct Student {

	int id;
	float ht;
	struct Student* next;
}Stud;

Stud* head = NULL;

void addNode() {

	Stud* newNode = (Stud*)malloc(sizeof(Stud));
	newNode->id = 1;
	newNode->ht = 5.5;
	newNode->next = NULL;
	head = newNode;
}
void printLL() {

	Stud* temp = head;
	while(temp != NULL) {

		printf("%d\n",temp->id);
		printf("%f\n",temp->ht);
		temp = temp->next;	
	}
}
void main() {

	addNode();
	addNode();
	printLL();
}
