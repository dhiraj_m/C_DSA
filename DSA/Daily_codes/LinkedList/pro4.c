#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	struct Node* prev;
	int data;
	struct Node* next;
}Node;

Node* head = NULL;
int countNode();
Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->prev = NULL;
	getchar();
	printf("Enter data\n");
	newNode->next = NULL;

	return newNode;
}

void addNode() {

	Node* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		Node* temp = head; 
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}

void addFirst() {

	Node* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}
}
void addAtPos(int pos) {

	int count = countNode();
	if(pos<=0 || pos>=count+2) {
	
		printf("Wrong Position\n");
	}else {

		if(pos == 1) {
		
			addFirst();
		}else if(pos == count+1) {
		
			addNode();
		}else {

			Node* newNode = createNode();
			Node* temp = head;

			while(pos-2) {
	
				temp = temp->next;
			}
			newNode->next = temp->next;
			newNode->prev = temp;
			temp->next->prev = newNode;
			temp->next = newNode;
		}
	}
}
void deleteFirst() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else if(head->next == NULL) {
	
		free(head);
		head == NULL;
	}else {
	
		head = head->next;
		free(head->prev);
		head->prev = NULL;
	}
}
void deleteLast() {

	if(head == NULL) {
	
		printf("No Node present\n");
	}else if(head->next == NULL) {
	
		free(head);
		head = NULL;
	}else {
	
		Node* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->prev->next = NULL;
		free(temp);
	}
}
void delAtPos(int pos) {

	int count = countNode();
	if(pos<=0 || pos>count) {
	
		printf("Wrong position\n");
	}else {
	
		if(pos == 1) {
		
			deleteFirst();
		}else if(pos == count) {
		
			deleteLast();
		}else {
		
			Node* temp = head;
			while(temp->next != NULL) {
			
				temp = temp->next;
			}
			temp->prev->next = temp->next;
			temp->next->prev = temp->prev;
		}
	}
}
int countNode() {

	int count = 0;
	Node* temp = head;
	while(temp != NULL) {
	
		count++;
		temp = temp->next;
	}
	return  count;
}
void printLL() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else {

		Node* temp = head;
		while(temp->next != NULL) {
	
			printf("|%d -> ",temp->data);
			temp = temp->next;
		}
		printf("%d|",temp->data);
	}
}
void main() {

	char choice;
	do {

		int n;
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.countNode\n");
		printf("6.delFirst\n");
		printf("7.delLast\n");
		printf("8.delAtPos\n");
		printf("9.printLL\n");

		printf("Enter your choice\n");
		scanf("%d",&n);

		switch(n) {
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addNode();
				break;
			case 4:
				{
					int pos2;
					printf("Enter position to add node\n");
					scanf("%d",&pos2);
					addAtPos(pos2);
				}
				break;
			case 5:
				countNode();
				break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			case 8:
				{
					int pos;
					printf("Enter Position to delete node\n");
					scanf("%d",&pos);
					delAtPos(pos);
				}
				break;
			case 9:
				printLL();
				break;
			default:
				printf("Invalid Input\n");
		}
		getchar();
		printf("Do you want continue?\n");
		scanf(" %c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
