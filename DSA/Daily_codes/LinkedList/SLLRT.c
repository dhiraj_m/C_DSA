#include<stdio.h>
#include<stdlib.h>

typedef struct PlayList {

	char name[20];
	float duration;
	struct PlayList* next;
}PL;

PL* head = NULL;

PL* createNode() {

	PL* newNode = (PL*)malloc(sizeof(PL));
	getchar();
	printf("Enter song name\n");
	int i = 0;
	char ch;
	while((ch = getchar()) != '\n') {
	
		newNode->name[i] = ch;
		i++;
	}
	printf("Enter duration of song\n");
	scanf("%f",&newNode->duration);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	PL* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		PL* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void addFirst() {

	PL* newNode = createNode();
	if(head == NULL) {
	
		head = newNode;
	}else {
	
		newNode->next = head;
		head = newNode;
	}
}
int countNode();
void addAtPos(int pos) {

	int count = countNode();
	if(pos == 1) {
	
		addFirst();
	}else if(pos>count+1 || pos<=0) {
	
		printf("Enter valid input\n");
	}else {
		PL* newNode = createNode();
		PL* temp = head;
		while(pos-2) {
			
			temp = temp->next;
			pos--;
		}
		newNode->next = temp->next;
		temp->next = newNode;
	}
}
int countNode() {

	int count = 0;
	PL* temp = head;
	while(temp != NULL) {
	
		count++;
		temp = temp->next;
	}
	return count;
}
void deleteFirst() {

	if(head == NULL) {
	
		printf("No node is present\n");
	}else {

		PL* temp = head;
		head = head->next;
		free(temp);
	}
}
void deleteLast() {

	if(head == NULL) {
	
		printf("No node is present\n");
	}else if(head->next == NULL) {
	
		deleteFirst();
	}else {

		PL* temp = head;
		while(temp->next->next != NULL) {
		
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}
void delAtPos(int pos) {

	int count = countNode();
	if(pos<=0 || pos>count) {
	
		printf("Invalid input\n");
	}else if(pos == 1) {
	
		deleteFirst();
	}else if(pos == count) {
	
		deleteLast();
	}else {
	
		PL* temp1 = head;
		PL* temp2 = head;
		while(pos-2) {
		
			temp1 = temp1->next;
			pos--;
		}
		temp2 = temp1->next;
		temp1->next = temp2->next;
		free(temp2);
	}
}
void printLL() {

	PL* temp = head;
	while(temp != NULL) {
	
		printf("|%s->",temp->name);
		printf("%f|",temp->duration);
		temp = temp->next;
	}
	printf("\n");
}
void main() {

	char choice;
	do {
	
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPos\n");
		printf("4.printLL\n");
		printf("5.count\n");
		printf("6.deleteFirst\n");
		printf("7.deleteLast\n");
		printf("8.deleteAtPos\n");
		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);

		switch(ch) {
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				{
				int pos;
				printf("Enter position\n");
				scanf("%d",&pos);
				addAtPos(pos);
				}
				break;
			case 4:
				printLL();
				break;
			case 5:
				printf("count is %d\n",countNode());
				break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			case 8:
				{
				int pos;
				printf("Enter postion\n");
				scanf("%d",&pos);
				delAtPos(pos);
				break;
				}
			default:
				printf("Enter valid input\n");
		}
		getchar();
		printf("\nDo you want to continue?\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
