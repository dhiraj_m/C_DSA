#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

Node* head = NULL;
int count();

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));

	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	Node* newNode = createNode();
	if(head == NULL) {
	
		head = newNode;
		newNode->next = head;
	}else {
	
		Node* temp = head;
		while(temp->next != head) {
		
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->next = head;
	}
}
int countNode() {

	if(head == NULL) {
	
		return 0;
	}else {

		int count = 1;
		Node* temp = head;

		while(temp->next != head) {
	
			count++;
			temp = temp->next;
		}
		return count;
	}
}
void addFirst() {

	Node* newNode = createNode();
	if(head == NULL) {
	
		head = newNode;
		newNode->next = head;
	}else {
	
		Node* temp = head;
		newNode->next = temp;
		while(temp->next != head) {
		
			temp = temp->next;
		}
		temp->next = newNode;
		head = newNode;
	}
}
int addAtPos(int pos) {

	int count = countNode();
	if(pos <= 0 || pos>count+1) {
	
		printf("Invalid position\n");
		return -1;
	}else {
	
		if(pos == 1) {
		
			addFirst();
		}else if(pos == count+1) {
		
			addNode();
		}else {
		
			Node* newNode = createNode();
			Node* temp = head;
			while(pos-2) {
			
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
		return 0;
	}
}
void delFirst() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else{
	
		Node* temp1 = head;
		Node* temp2 = head;
		while(temp2->next != head) {
	
			temp2 = temp2->next;
		}
		head = head->next;
		temp2->next = head;
		free(temp1);
	}
}
void delLast() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else if(head->next == head){
	
		free(head);
		head = NULL;
	}else {
	
		Node* temp = head;
		while(temp->next->next != head) {
		
			temp = temp->next;
		}
		free(temp->next);
		temp->next = head;
	}
}
void delAtPos(int pos) {

	int count = countNode();
	if(pos<=0 || pos>count) {
	
		printf("Invalid position\n");
	}else {
	
		if(pos == 1) {
		
			delFirst();
		}else if(pos == count) {
		
			delLast();
		}else {
		
			Node* temp1 = head;
			Node* temp2 = head;
			while(pos-2) {
			
				temp1 = temp1->next;
				pos--;
			}
			temp2 = temp1->next;
			temp1->next = temp2->next;
			free(temp2);
		}
	}
}
void printSCLL(){

	if(head == NULL) {
	
		printf("No node present\n");
	}else {

		Node* temp = head;
		while(temp->next != head) {
		
			printf("|%d|->",temp->data);
			temp = temp->next;
		}	
		printf("|%d|\n",temp->data);
	}
}
void main() {

	char choice;

	do{
	
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPos\n");
		printf("4.addlast\n");
		printf("5.count\n");
		printf("6.delFirst\n");
		printf("7.delLast\n");
		printf("8.delAtPos\n");
		printf("9.printSCLL\n");
		int i;
		printf("Enter your choice\n");
		scanf("%d",&i);
		switch(i) {
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				{
					int pos;
					printf("Enter position\n");
					scanf("%d",&pos);
					addAtPos(pos);
				}
				break;
			case 4:
				addNode();
				break;
			case 5:
				printf("count is %d\n",countNode());
				break;
			case 6:
				delFirst();
				break;
			case 7:
				delLast();
				break;
			case 8:
				{
					int pos;
					printf("Enter position to delete\n");
					scanf("%d",&pos);
					delAtPos(pos);
				}	
				break;
			case 9:
				printSCLL();
				break;
			default:
				printf("Enter valid input\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
	printSCLL();
}
