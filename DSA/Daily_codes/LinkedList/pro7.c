#include<stdio.h>
#include<string.h>

struct Company {

	int empCount;
	char name[20];
	float rev;
	struct Company *next;
};
void main() {

	struct Company obj1,obj2,obj3;
	struct Company *head = &obj1;

	head->empCount = 100;
	strcpy(head->name,"TCS");
	head->rev = 100.00;
	head->next = &obj2;
	
	head->next->empCount = 50;
	strcpy(head->next->name,"Accenture");
	head->next->rev = 50.00;
	head->next->next = &obj3;
	
	head->next->next->empCount = 500;
	strcpy(head->next->next->name,"Biencaps");
	head->next->next->rev = 1000.00;
	head->next->next->next = NULL;

	printf("%d\n",head->empCount);
	printf("%s\n",head->name);
	printf("%f\n",head->rev);
	
	printf("%d\n",head->next->empCount);
	printf("%s\n",head->next->name);
	printf("%f\n",head->next->rev);
	
	printf("%d\n",head->next->next->empCount);
	printf("%s\n",head->next->next->name);
	printf("%f\n",head->next->next->rev);
}
