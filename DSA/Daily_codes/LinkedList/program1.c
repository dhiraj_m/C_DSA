#include<stdio.h>
#include<stdlib.h>

typedef struct Demo {

	int data;
	struct Demo* next;
}dm;

dm* head = NULL;

dm* createNode() {

	dm* newNode = (dm*)malloc(sizeof(dm));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	dm* newNode = createNode();

//	printf("Ith nahi\n");
	if(head == NULL) {
	
		head = newNode;
		printf("Ith nahi\n");
	}else {
	
		dm* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void addFirst() {

	dm* newNode = createNode();
	if(head == NULL) {
	
		head = newNode;
	}else {
	
		newNode->next = head;
		head = newNode;
	}
}
int countNode();
void addAtPos(int pos) {

	int count = countNode();
	if(pos == 1) {
	
		addFirst();
	}else if(pos>count+1 || pos<=0) {
	
		printf("Enter valid input\n");
	}else {
		dm* newNode = createNode();
		dm* temp = head;
		while(pos-2) {
			
			temp = temp->next;
			pos--;
		}
		newNode->next = temp->next;
		temp->next = newNode;
	}
}
int countNode() {

	int count = 0;
	dm* temp = head;
	while(temp != NULL) {
	
		count++;
		temp = temp->next;
	}
	return count;
}
void deleteFirst() {

	dm* temp = head;
	head = head->next;
	free(temp);
}
void deleteLast() {

	dm* temp = head;
	while(temp->next->next != NULL) {
	
		temp = temp->next;
	}
	free(temp->next);
	temp->next = NULL;
}
void printLL() {

	dm* temp = head;
	while(temp != NULL) {
	
		printf("| %d |",temp->data);
		temp = temp->next;
	}
}
void main() {

	char choice;
	do {
	
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPos\n");
		printf("4.printLL\n");
		printf("5.count\n");
		printf("6.deleteFirst\n");
		printf("7.deleteLast\n");
		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);

		switch(ch) {
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				{
				int pos;
				printf("Enter position\n");
				scanf("%d",&pos);
				addAtPos(pos);
				}
				break;
			case 4:
				printLL();
				break;
			case 5:
				printf("count is %d\n",countNode());
				break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			default:
				printf("Enter valid input\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
