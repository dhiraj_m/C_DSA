#include <stdio.h>

int countStep(int num) {

	if(num == 0) 
		return 0;

	if(num%2 == 0) 
		return 1 + countStep(num/2);

	if(num%2 == 1) 
		return 1 + countStep(num-1);
}

void main() {

	int num;
	printf("Enter number\n");
	scanf("%d",&num);

	printf("Number of steps are %d\n",countStep(num));
}
