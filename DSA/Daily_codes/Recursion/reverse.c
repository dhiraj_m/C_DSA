#include <stdio.h>

int reverseNum(int num) {

	static int rev = 0;
	rev = rev*10 + num%10;
	if(num == 0)
		return rev;

	return reverseNum(num/10);
}

void main() {

	int num;
	printf("Enter number\n");
	scanf("%d",&num);

	printf("Num in reverse is %d\n",reverseNum(num));
}
