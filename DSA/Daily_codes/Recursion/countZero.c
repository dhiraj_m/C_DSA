#include <stdio.h>

int countZero(int num) {

	static int flag = 1;
	if(num == 0) {
	
		return flag;
	}
	if(num%10 == 0) {
		
		flag = 0;
		return 1 + countZero(num/10);
	}
	return countZero(num/10);
}

void main() {

	int num;
	printf("Enter number\n");
	scanf("%d",&num);
	printf("Number of zeros are %d\n",countZero(num));
}
