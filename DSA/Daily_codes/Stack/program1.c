#include <stdio.h>
#include <stdbool.h>

int top = -1;
int size = 0;


bool isFull() {

	if(top == size-1)
		return true;
	else
		return false;
}
int push(int stack[]) {

	if(isFull())
		return -1;
	else {
	
		printf("Enter data\n");
		scanf("%d",&stack[++top]);
		return 0;
	}
}
bool isEmpty() {

	if(top == -1)
		return true;
	else
		return false;
}
int pop(int stack[]) {

	if(isEmpty())
		return -1;
	else {
	
		int val = stack[top];
		top--;
		return val;
	}
}
int peek(int stack[]) {

	return stack[top];
}
void main() {

	char choice;
	printf("Enter size of array\n");
	scanf("%d",&size);
	int stack[size];

	do {
	
		printf("1.push\n");
		printf("2.pop\n");
		printf("3.peek\n");
		printf("4.isFull\n");
		printf("5.isEmpty\n");
		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);

		switch(ch) {
		
			case 1:
				{
				int ret = push(stack);
				if(ret == -1)
					printf("Stack Overflow\n");
				}
				break;
			case 2:
				{
				int ret = pop(stack);
				if(isEmpty())
					printf("Stack Underflow\n");
				else 
					printf("%d is popped\n",ret);
				}
				break;
			case 3:
				{
				if(isEmpty())
					printf("No ele present\n");
				else
					printf("%d",peek(stack));
				}
				break;
			case 4:
				{
				if(isFull())
					printf("Stack is full\n");
				else
					printf("Stack is not full\n");
				}
				break;
			case 5:
				{
				if(isEmpty()) 
					printf("Stack is empty\n");
				else
					printf("Stack is not empty");
				}
				break;
			default:
				printf("Invalid choice\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
