#include <stdio.h>

int top1 = -1;
int top2 = -1;
int size = 0;
int flag = 0;

int push1(int stack[]) {

	if(top2-top1 == 1)
		return -1;
	else {
	
		top1++;
		printf("Enter data(1st stack)\n");
		scanf("%d",&stack[top1]);
	}
}
int push2(int stack[]) {

	if(top2-top1 == 1)
		return -1;
	else {
	
		top2--;
		printf("Enter data(2nd stack)\n");
		scanf("%d",&stack[top2]);
	}
}
int pop1(int stack[]) {

	if(top1 == -1) {

		flag = 0;
		return -1;
	} else {
	
		flag = 1;
		int val = stack[top1];
		top1--;
		return val;
	}
}
int pop2(int stack[]) {

	if(top2 == size) {

		flag = 0;
		return -1;
	} else {
	
		flag = 1;
		int val = stack[top2];
		top2++;
		return val;
	}
}
void main() {

	printf("Enter size\n");
	scanf("%d",&size);

	top1 = -1;
	top2 = size;

	int stack[size];
	char choice;

	do {
	
		printf("1.push1\n");
		printf("2.push2\n");
		printf("3.pop1\n");
		printf("4.pop2\n");
		int i;
		printf("Enter your choice\n");
		scanf("%d",&i);

		switch(i) {
		
			case 1:
				{
				int retVal = push1(stack);
				if(retVal == -1)
					printf("Stack Overflow\n");
				}
				break;
			case 2:
				{
				int retVal = push2(stack);
				if(retVal == -1)
					printf("Stack Overflow\n");
				}
				break;
			case 3:
				{
				int retVal = pop1(stack);
				if(flag == 1)
					printf("%d popped\n",retVal);
				else
					printf("Stack Overflow");
				}
				break;
			case 4:
				{
				int retVal = pop2(stack);
				if(flag == 1)
					printf("%d popped\n",retVal);
				else
					printf("Stack Overflow");
				}
				break;
			default:
				printf("Invalid choice\n");
		}
		getchar();
		printf("Do you want to continue\n");
		scanf("%c",&choice);
	} while(choice == 'Y' || choice == 'y');
}

