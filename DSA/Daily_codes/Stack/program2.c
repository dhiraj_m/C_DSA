#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct Demo {

	int data;
	struct Demo* next;
}dm;

dm* head = NULL;
int flag = 0;
dm* top = NULL;
int size = 0;

dm* createNode() {

	dm* newNode = (dm*)malloc(sizeof(dm));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode() {

	dm* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		top = head;
		while(top->next != NULL) {
		
			top = top->next;
		}
		top->next = newNode;
	}
}
int eleCount();
bool isFull() {

	if(eleCount() == size)
		return true;
	else
		return false;
}
bool isEmpty() {

	if(eleCount() == 0)
		return true;
	else
		return false;
}
int eleCount() {

	int count = 0;
	top = head;
	while(top != NULL) {
	
		count++;
		top = top->next;
	}
	return count;
}
int deleteLast() {

	if(head->next == NULL) {
	
		int val = head->data;
		free(head);
		head = NULL;
		return val;
	}else {

		top = head;
		while(top->next->next != NULL) {
		
			top = top->next;
		}
		int data = top->next->data;
		free(top->next);
		top->next = NULL;
		return data;
	}
}
int push() {

	if(isFull()) {
	
		return -1;
	}else {
	
		addNode();
		return 0;
	}
}
int pop() {

	if(isEmpty()) {
	
		flag = 0;
		return -1;
	}else {
	
		flag = 1;
		return deleteLast();
	}
}
int peek() {

	if(isEmpty()) {

		flag = 0;
		return -1;
	}
	else {
		flag = 1;
		top = head;
		while(top->next != NULL) {
		
			top = top->next;
		}
		return top->data;
	}
}
void main() {

	char choice;
	printf("Enter num of nodes\n");
	scanf("%d",&size);

	do {
	
		printf("1.push\n");
		printf("2.pop\n");
		printf("3.peek\n");
		printf("4.isFull\n");
		printf("5.isEmpty\n");
		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);

		switch(ch) {
		
			case 1:
				{
				int ret = push();
				if(ret == -1)
					printf("Stack Overflow\n");
				}
				break;
			case 2:
				{
				int ret = pop();
				if(flag == 0)
					printf("Stack Underflow\n");
				else 
					printf("%d is popped\n",ret);
				}
				break;
			case 3:
				{
				int ret = peek();
				if(flag == 0)
					printf("No ele present\n");
				else
					printf("%d\n",ret);
				}
				break;
			case 4:
				{
				if(isFull())
					printf("Stack is full\n");
				else
					printf("Stack is not full\n");
				}
				break;
			case 5:
				{
				if(isEmpty()) 
					printf("Stack is empty\n");
				else
					printf("Stack is not empty\n");
				}
				break;
			default:
				printf("Invalid choice\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
