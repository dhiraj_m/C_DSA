#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

Node* head = NULL;

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	printf("%p\n",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
int addNode() {

	Node* newNode = createNode();
	if(head == NULL) {
	
		head = newNode;
	} else {
	
		Node* temp1 = head;
		while(temp1->next != NULL) {
		
			temp1 = temp1->next;
		}
		if(head->data > newNode->data) {
		
			newNode->next = head;
			head = newNode;
		} else if(temp1->data < newNode->data) {
		
			temp1->next = newNode;
		} else {
		
			Node* temp2 = head;
			while(temp2->next != NULL && temp2->next->data <= newNode->data) {
			
				temp2 = temp2->next;
			}
			newNode->next = temp2->next;
			temp2->next = newNode;
		}
		return 0;
	}	
}
int delFirst() {

	if(head == NULL) {
	
		printf("No Node present\n");
		return -1;
	} else {
		
		Node* temp = head;
		head = head->next;
		free(temp);
		return 0;
	}
}
int printLL() {

	if(head == NULL) {
	
		printf("LinkedList is empty\n");
		return -1;
	} else {
	
		Node* temp = head;
		while(temp->next != NULL) {
		
			printf("%d->%p->",temp->data,&temp->data);
			temp = temp->next;
		}
		printf("%d->%p\n",temp->data,&temp->data);
		return 0;
	}
}
void main() {

	char choice;

	do {
	
		printf("1.addNode\n");
		printf("2.delFirst\n");
		printf("3.printLL\n");
		int i;
		printf("Enter choice\n");
		scanf("%d",&i);
		switch(i) {
		
			case 1:
				addNode();
				break;
			case 2:
				delFirst();
				break;
			case 3:
				printLL();
				break;
			default:
				printf("Invalid choice\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	} while(choice == 'Y' || choice == 'y');
}
