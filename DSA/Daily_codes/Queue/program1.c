#include <stdio.h>

int front = -1;
int rear = -1;
int size = 0;
int flag = 0;

int enqueue(int que[]) {

	if(rear == size-1)
		return -1;
	else {
	
		if(front == -1) {

			front++;
		}
		rear++;
		printf("Enter data\n");
		scanf("%d",&que[rear]);
		return 0;
	}
}
int dequeue(int que[]) {

	if(front == -1 || front>rear) {

		flag = 0;
		return -1;
	}else {

		flag = 1;
		int val = que[front];
		front++;
		return val;
	}
}
int frontt(int que[]) {

	if(front == -1 || front>rear) {

		flag = 0;
		return -1;
	}else {
	
		flag = 1;
		return que[front];
	}
}
int printQueue(int que[]) {

	if(front == -1) {

		printf("Q is empty\n");
		return -1;
	}else {
	
		for(int i=0;i<=rear; i++) {
		
			printf("%d  ",que[i]);
		}
		printf("\n");
		return 0;
	}
}
void main() {

	char choice;
	printf("Enter size of array\n");
	scanf("%d",&size);
	int que[size];

	do {
	
		printf("1.Enqueue\n");
		printf("2.Dequeue\n");
		printf("3.Front\n");
		printf("4.printQueue\n");
		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);

		switch(ch) {
		
			case 1:
				{
				int ret = enqueue(que);
				if(ret == -1)
					printf("Q Overflow\n");
				}
				break;
			case 2:
				{
				int ret = dequeue(que);
				if(flag == 0)
					printf("Q Underflow\n");
				else
					printf("%d dequeued\n",ret);
				}
				break;
			case 3:
				{
				int ret = frontt(que);
				if(flag == 0)
					printf("No element\n");
				else 
					printf("%d\n",ret);
				}
				break;
			case 4:
				printQueue(que);
				break;
			default:
				printf("Invalid choice\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
