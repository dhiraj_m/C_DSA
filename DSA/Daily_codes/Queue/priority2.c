#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	int data;
	int priority;
	struct Node* next;
}Node;

Node* head = NULL;

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	do {
	
		printf("Enter priority\n");
		scanf("%d",&newNode->priority);
	} while(newNode->priority > 5 || newNode->priority < 0);
	newNode->next = NULL;

	return newNode;
}
int addNode() {

	Node* newNode = createNode();
	if(head == NULL) {
	
		head = newNode;
		return -1;
	} else {
	
		Node* temp1 = head;
		while(temp1->next != NULL) {
		
			temp1 = temp1->next;
		}
		if(head->priority < newNode->priority) {
		
			newNode->next = head;
			head = newNode;
		} else if(temp1->priority > newNode->priority) {
		
			temp1->next = newNode;
		} else {
		
			Node* temp2 = head;
			while(temp2->next != NULL && temp2->next->priority >= newNode->priority) {
			
				temp2 = temp2->next;
			}
			newNode->next = temp2->next;
			temp2->next = newNode;
		}
		return 0;
	}	
}
int delFirst() {

	if(head == NULL) {
	
		printf("No Node present\n");
		return -1;
	} else {
		
		Node* temp = head;
		head = head->next;
		free(temp);
		return 0;
	}
}
int printLL() {

	if(head == NULL) {
	
		printf("LinkedList is empty\n");
		return -1;
	} else {
	
		Node* temp = head;
		while(temp->next != NULL) {
		
			printf("%d->%d->",temp->data,temp->priority);
			temp = temp->next;
		}
		printf("%d->%d\n",temp->data,temp->priority);
		return 0;
	}
}
void main() {

	char choice;

	do {
	
		printf("1.addNode\n");
		printf("2.delFirst\n");
		printf("3.printLL\n");
		int i;
		printf("Enter choice\n");
		scanf("%d",&i);
		switch(i) {
		
			case 1:
				addNode();
				break;
			case 2:
				delFirst();
				break;
			case 3:
				printLL();
				break;
			default:
				printf("Invalid choice\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	} while(choice == 'Y' || choice == 'y');
}
