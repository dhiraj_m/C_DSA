#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	int data;
	int priority;
	struct Node* next;
}Node;

Node* front = NULL;
Node* rear = NULL;
int flag = 0;

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	do {
	
		printf("Enter priority\n");
		scanf("%d",&newNode->priority);
	} while(newNode->priority > 5 || newNode->priority < 0);
	newNode->next = NULL;

	return newNode;
}
int enqueue() {

	Node* newNode = createNode();
	if(newNode == NULL) {
	
		printf("Memory full\n");
		return -1;
	} else {
	
		if(front == NULL) {
	
			front = newNode;
			rear = newNode;
		} else if(front->priority < newNode->priority) {
		
			newNode->next = front;
			front = newNode;
		} else if(rear->priority > newNode->priority) {
		
			rear->next = newNode;
		} else {
		
			Node* temp = front;
			while(temp->next != NULL && temp->next->priority >= newNode->priority) {
			
				temp = temp->next;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
		return 0;
	}	
}
int dequeue() {

	if(front == NULL) {
	
		flag = 0;
		printf("No Node present\n");
		return -1;
	} else {
		
		flag = 1;
		int val = front->data;
		if(front->next == NULL) {

			free(front);
			front = NULL;
		} else {

			Node* temp = front;
			front = front->next;
			free(temp);
		}
		return val;
	}
}
int printLL() {

	if(front == NULL) {
	
		printf("LinkedList is empty\n");
		return -1;
	} else {
	
		Node* temp = front;
		while(temp->next != NULL) {
		
			printf("%d->%d->",temp->data,temp->priority);
			temp = temp->next;
		}
		printf("%d->%d\n",temp->data,temp->priority);
		return 0;
	}
}
void main() {

	char choice;

	do {
	
		printf("1.addNode\n");
		printf("2.delFirst\n");
		printf("3.printLL\n");
		int i;
		printf("Enter choice\n");
		scanf("%d",&i);
		switch(i) {
		
			case 1:
				{
				int ret = enqueue();
				if(ret == -1)
					printf("Queue overflow\n");
				}
				break;
			case 2:
				{
				int ret = dequeue();
				if(flag == 0)
					printf("Queue underflow\n");
				else 
					printf("%d dequeued\n",ret);
				}
				break;
			case 3:
				printLL();
				break;
			default:
				printf("Invalid choice\n");
		}
		getchar();
		printf("Do you want to continue?\n");
		scanf("%c",&choice);
	} while(choice == 'Y' || choice == 'y');
}
