#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

Node* front = NULL;
Node* rear = NULL;
int count = 0;
int flag = 0;
int size = 0;

Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
int enqueue() {

	Node* newNode = createNode();

	if(front == NULL) {

		front = newNode;
		rear = newNode;
	}
	rear->next = newNode;
	rear = newNode;

	count++;
	return 0;
}
int deQ() {

	if(front == NULL) {

		flag = 0;	
		return -1;
	} else {
	
		flag = 1;
		int val = front->data;
		if(front->next == NULL) {
		
			free(front);
			front = NULL;
		} else {
		
			Node* temp = front;
			front = front->next;
			free(temp);
		}
		count--;
		return val;
	}
}
int frontt() {

	return front->data;
}
int printQ() {

	Node* temp = front;
	while(temp->next!= NULL) {
	
		printf("%d->",temp->data);
		temp = temp->next;
	}
	printf("%d\n",temp->data);
}
void main() {

	char choice;
	printf("Enter size of linkedList\n");
	scanf("%d",&size);
	do {
	
		printf("1.enqueue\n");
		printf("2.dequeue\n");
		printf("3.front\n");
		printf("4.printQ\n");
		int i;
		printf("Enter your choice\n");
		scanf("%d",&i);

		switch(i) {
		
			case 1:
				{
				if(count == size)
					printf("Stack Overflow\n");
				else 
					enqueue();
				}
				break;
			case 2:
				{
				int retVal = deQ();
				if(flag == 1)
					printf("%d dequeued\n",retVal);
				else
					printf("Stack Underflow\n");
				}
				break;
			case 3:
				{
				if(count == 0)
					printf("No node present\n");
				else
					printf("%d\n",frontt());
				}
				break;
			case 4:
				{
				if(count == 0) 
					printf("Q is empty\n");
				else
					printQ();
				}
				break;
			default:
				printf("Invalid choice\n");
		}
		getchar();
		printf("Continue?\n");
		scanf("%c",&choice);
	} while(choice == 'Y' || choice == 'y');
}
