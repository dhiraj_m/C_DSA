#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node* next;
}Node;

Node* head = NULL;

int countNode() {

	int count = 0;
	Node* temp = head;
	while(temp != NULL) {
	
		count++;
		temp = temp->next;
	}
	return count;
}


Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;
	return newNode;
}
void addNode() {

	Node* newNode = createNode();
	if(head == NULL) {
	
		head = newNode;
	}else {
	
		Node* temp = head;
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void reverseSLL() {

	Node* temp = head;
	int cnt1 = countNode();
	int cnt2 = countNode();
	while(cnt1-1) {
	
		Node* temp = head;
		while(cnt2-1) {
		
			int temp2 = temp->data;
			temp->data = temp->next->data;
			temp->next->data = temp2;
			temp = temp->next;	
			cnt2--;
		}
		cnt1--;
		cnt2 = cnt1;
	}
}
void printLL() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else {
	
		Node* temp = head;

		while(temp->next != NULL) {
	
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
void main() {

	int nodes;
	printf("Enter number of nodes\n");
	scanf("%d",&nodes);
	
	for(int i=0; i<nodes; i++) {
	
		addNode();
	}
	printLL();
	if(nodes>=1) {
	
		reverseSLL();
		printLL();
	}
}
