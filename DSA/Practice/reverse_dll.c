#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	struct Node* prev;
	int data;
	struct Node* next;
}Node;

Node* head = NULL;

int countNode() {

	int count = 0;
	Node* temp = head;
	while(temp != NULL) {
	
		count++;
		temp = temp->next;
	}
	return count;
}
Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->prev = NULL;
	getchar();
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

void addNode() {

	Node* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		Node* temp = head; 
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}
void printLL() {

	if(head == NULL) {
	
		printf("No node present\n");
	}
	else {

		Node* temp = head;
		while(temp->next != NULL) {
	
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
void reverseDLL() {

	if(head == NULL){
	
		printf("No node present\n");
	}else {

		int cnt = countNode()/2;
		Node* temp1 = head;
		Node* temp2 = head;
		while(temp2->next != NULL) {
		
			temp2 = temp2->next;
		}
		while(cnt) {
	
			int temp = temp1->data;
			temp1->data = temp2->data;
			temp2->data = temp;
			temp1 = temp1->next;
			temp2 = temp2->prev;
			cnt--;
		}
	}
}
void main() {

	int node;
	printf("Enter number of nodes\n");
	scanf("%d",&node);
	for(int i=0; i<node; i++) {
	
		addNode();
	}
	printLL();
	if(node>=1) {
	
		reverseDLL();
		printLL();
	}
}
