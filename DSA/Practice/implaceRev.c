#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	struct Node* prev;
	int data;
	struct Node* next;
}Node;

Node* head = NULL;
int countNode();
Node* createNode() {

	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->prev = NULL;
	getchar();
	printf("Enter data\n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

void addNode() {

	Node* newNode = createNode();

	if(head == NULL) {
	
		head = newNode;
	}else {
	
		Node* temp = head; 
		while(temp->next != NULL) {
		
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}
void reverse() {

	if(head == NULL) {
	
		printf("Empty\n");
	}else {
	
		Node* temp = head;
		while(temp->next != NULL){
		
			Node* temp2 = temp->next;
			temp->next = temp->prev;
			temp->prev = temp2;
			temp = temp->prev;
		}
		temp->next = temp->prev;
		temp->prev = NULL;
		head = temp;
	}
}
int countNode() {

	int count = 0;
	Node* temp = head;
	while(temp != NULL) {
	
		count++;
		temp = temp->next;
	}
	return  count;
}
void printLL() {

	if(head == NULL) {
	
		printf("No node present\n");
	}else {

		Node* temp = head;
		while(temp->next != NULL) {
	
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}
void main() {

	int node;
	printf("Enter num of nodes\n");
	scanf("%d",&node);

	for(int i=0; i<node; i++) {
	
		addNode();
	}
	printLL();
	reverse();
	printLL();
}
