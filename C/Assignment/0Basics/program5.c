#include<stdio.h> 

void main() {

	int x;
	
	printf("Enter number\n");
	scanf("%d",&x);

	if(x%5 == 0 && x%11 == 0) {
	
		printf("The number %d is divisible by 5 and 11\n",x);
	}
	else {

		printf("The number %d is not divisible by 5 and 11\n",x);
	}
}
