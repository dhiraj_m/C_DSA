#include<stdio.h> 

void main() {

	char ch;
	printf("Enter character\n");
	scanf("%c",&ch);

	if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
	
		printf("Entered character is vowel\n");
	}
	else if(ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U') {
	
		printf("%c is vowel\n",ch);
	}
	else {
	
		printf("%c is a consonant",ch);
	}
}
