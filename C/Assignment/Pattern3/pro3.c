#include<stdio.h>

void main() {

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++) {
	
		int a = rows;
		char ch = 'a';

		for(int j=1; j<=rows; j++) {
		
			if(j%2 == 1) {
			
				printf("%d\t",a);
				a--;
			}else {
			
				printf("%c\t",ch);
				ch++;
			}
		}
		printf("\n");
	}
}
