#include<stdio.h>

void main() {

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++) {
	
		char ch1 = 'a'+i-1;
		char ch2 = 'A'+i-1;

		for(int j=1; j<=rows; j++) {
		
			if(j%2 == 0) {

				printf("%c\t",ch2);
			
			}else {
			
				printf("%c\t",ch1);
			}
			ch1++;
			ch2++;
		}
		printf("\n");
	}
}
