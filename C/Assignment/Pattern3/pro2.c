
#include<stdio.h>

void main() {

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++) {
	
		int a = rows-1;
		char ch = 'a';

		for(int j=1; j<=rows; j++) {
		
			if(i%2 == 0) {
			
				if(j%2 == 0) {
				
					printf("%d\t",a);
				}else {
				
					printf("%c\t",ch);
				}
			}
			else {
			
				if(j%2 == 0) {
				
					printf("%c\t",ch);
				}else {
				
					printf("%d\t",a);
				}
			}
			a--;
			ch++;
		}
		printf("\n");
	}
}
