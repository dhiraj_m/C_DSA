#include<stdio.h>

void main() {

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	int a = 0;
	char ch1 = 'A'+rows-1;
	char ch2 = 'a'+rows;
	for(int i = 1; i<=rows; i++) {

		ch1 = 'A'+rows-2+i;
		ch2 = 'a'+rows+i-2;

		for(int j=1; j<=rows; j++) {
		
			if(i%2 == 0) {
			
				printf("%c%d\t",ch2,a);
				ch2++;
				a--;
			}else {
			
				a++;
				printf("%c%d\t",ch1,a);
				ch1--;
			}
			
		}
		a++;
		printf("\n");
	}
}
