#include<stdio.h>

void main() {

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	int a = rows*rows;
	char ch = 'A'+rows*rows-1;
	for(int i = 1; i<=rows; i++) {
	
		for(int j=1; j<=rows; j++) {
		
			if(i%2 == 0) {
			
				printf("%c\t",ch);
			}else {
			
				printf("%d\t",a);
			}
			ch--;
			a--;
		}
		printf("\n");
	}
}
