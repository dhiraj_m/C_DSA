
#include<stdio.h>

void main() {

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++) {
	
		int a = i;

		for(int j=1; j<=rows; j++) {
		
			if(a%2 == 0) {
			
				printf("%d\t",a*a);
			}
			else {

				printf("%d\t",a*a*a);
			}
			a++;
		}
		printf("\n");
	}
}
