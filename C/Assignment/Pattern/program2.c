/*
 
   	1	2	3	
	a	b	c	
	3	4	5	
	a	b	c	

*/

#include<stdio.h>

void main() {

	int rows;
	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++) {
	
		int x = 1;
		char ch = 'a';

		for(int j=1; j<=3; j++) {
		
			if(i%2 == 0)
				printf("%c  ",ch);
			else
				printf("%d  ",x);
			x++;
			ch++;
		}
		printf("\n");
	}
}

