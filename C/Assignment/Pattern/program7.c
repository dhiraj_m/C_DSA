/*
 
   		1	2	9	4
		25	6	49	8
		81	10	121	12
		169	14	225	16

*/

#include<stdio.h>

void main() {

	int rows;
	printf("Enter rows\n");
	scanf("%d",&rows);

	int x = 1;

	for(int i=1; i<=rows; i++) {
	
		for(int j=1; j<=rows; j++) {
		
			if(x%2 == 0)
				printf("%d\t",x);
			else
				printf("%d\t",x*x);
			x++;
		}
		printf("\n");
	}
}

