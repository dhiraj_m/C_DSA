/*
 
   		D4	C3	B2	A1
		D4	C3	B2	A1
		D4	C3	B2	A1
		D4	C3	B2	A1

*/

#include<stdio.h>

void main() {

	int rows;
	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++) {
	
		int x = rows;
		char ch = 'A' + rows -1;

		for(int j=1; j<=rows; j++) {
		
			printf("%c%d  ",ch,x);
			ch--;
			x--;
		}
		printf("\n");
	}
}

