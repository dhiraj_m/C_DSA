/*
 
   		A	B	C	D
		B	C	D	E
		C	D	E	F
		D	E	F	G

*/

#include<stdio.h>

void main() {

	int rows;
	printf("Enter rows\n");
	scanf("%d",&rows);

	for(char ch='A'; ch<rows+'A'; ch++) {
	
		char x = ch;
		for(int j=1; j<=rows; j++) {
		
			printf("%c  ",x);
			x++;
		}
		printf("\n");
	}
}

