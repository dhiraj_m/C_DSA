/*
 
   		2	5	10
		17	26	37
		50	65	82

*/

#include<stdio.h>

void main() {

	int rows;
	printf("Enter rows\n");
	scanf("%d",&rows);

	int x = 2;
	int y = 3;

	for(int i=1; i<=rows; i++) {
	
		for(int j=1; j<=rows; j++) {
		
			printf("%d\t",x);
			x = x + y;
			y = y + 2;
		}
		printf("\n");
	}
}

