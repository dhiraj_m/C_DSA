#include<stdio.h> 

void main() {

	int num;
	printf("enter number between 1 and 5\n");
	scanf("%d",&num);

	switch(num) {
	
		case 0:
			printf("Zero\n");
			break;
		case 1:
			printf("One\n");
			break;
		case 2:
			printf("Two\n");
			break;
		case 3:
			printf("Three\n");
			break;
		case 4:
			printf("Four\n");
			break;
		case 5:
			printf("Five\n");
			break;
		default:
			printf("Entered number is greater than 5\n");
	}
}
