/*
	WAP to print a table of 11 in reverse
*/

#include <stdio.h>

void main() {

	int a;
	printf("Enter number\n");
	scanf("%d",&a);
	printf("Table of %d is\n",a);

	for(int i = 10; i>=1; i--) {
	
		printf("%d*%d = %d\n",a,i,a*i);
	}
}
