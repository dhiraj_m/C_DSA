/*
	WAP to print product of first 10 numbers
*/

#include <stdio.h>

void main() {

	int mult = 1;

	for(int i = 1; i<=10; i++) {
	
		mult = mult*i;
	}
	printf("Product of first 10 numbers is %d\n",mult);
}
