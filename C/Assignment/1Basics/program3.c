/*
 	WAP to print the first ten, 3 digit numbers
*/

#include <stdio.h>

void main() {

	for(int i = 100; i<=109; i++) {
	
		printf("%d\n",i);
	}
}
