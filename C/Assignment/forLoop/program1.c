/*
 	WAP to see given number is multiple of 3 

*/


#include<stdio.h>

void main() {

	int num;
	printf("Enter the number\n");
	scanf("%d",&num);

	if(num%3 == 0) {
	
		printf("The number is divisible by 3\n");
	}else {
	
		printf("The number is not divisible by 3\n");
	}
}
