/*
 	WAP to find the sum of numbers that are divisible by 5 in the given range

*/


#include<stdio.h>

void main() {

	int start,end;
	int sum = 0;

	printf("Enter start\n");
	scanf("%d",&start);
	
	printf("Enter end\n");
	scanf("%d",&end);

	for(int i=start; i<=end; i++) {
	
		if(i%5 == 0) {
		
			sum = sum + i;
		}
	} 
	printf("Sum of numbers divible by 5 is %d \n",sum);
}
