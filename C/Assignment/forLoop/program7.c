/*
 	WAP to calculate LCM of given two integers

*/


#include<stdio.h>

void main() {

	int num1,num2,lcm;
	int max;
	printf("Enter first number\n");
	scanf("%d",&num1);
	
	printf("Enter second number\n");
	scanf("%d",&num2);

	if(num1 > num2) {
	
		max = num1;
	}
	else {
	
		max = num2;
	}

	while(1) {
	
		if(max%num1 == 0 && max%num2 == 0) {
		
			lcm = max;
			break;
		}
		max++;
	}
	printf("LCM of %d and %d is %d \n",num1,num2,lcm);
}
