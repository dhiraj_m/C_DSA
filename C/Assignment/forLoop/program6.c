/*
 	WAP to calculate the factorial of given number

*/


#include<stdio.h> 

void main() {

	int num;
	int fact = 1;
	printf("Enter number\n");
	scanf("%d",&num);

	for(int i = 1;i <= num; i++) {
	
		fact = fact*i;
	}
	printf("Factorial of %d is %d \n",num,fact);
}
