/*
 
   WAP to print the numbers in given range and their multiplicative inverse
*/


#include<stdio.h>

void main() {

	int start,end;
	
	printf("Enter start\n");
	scanf("%d",&start);
	
	printf("Enter end\n");
	scanf("%d",&end);

	for(float i = start; i<=end; i++) {

		float m = 1/i;
		printf("%0.1f = %0.2f\n",i,m);
	}
}
