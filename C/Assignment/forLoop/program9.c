/*
 	WAP to print the square root of number ranging from 100 to 300

*/

#include<stdio.h>

void main() {

	int j = 0;
	float root = 1;

	for(int i = 100; i<=300; i++) {
	
		while(1) {
		
			root = (i/root + root)/2;
			j++;
			if(j > i) {
			
				break;
			}
		}
		printf("Square root of %d is %0.2f \n",i,root);
	}
}
