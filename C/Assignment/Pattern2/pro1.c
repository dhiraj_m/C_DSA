/*
		
   	4	3	2	1
	5	4	3	2
	6	5	4	3
	7	6	5	4

*/


#include<stdio.h>

void main() {

	int rows;
	printf("Enter no. of rows\n");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++) {
	
		int x = rows+i-1; 

		for(int j=1; j<=rows; j++) {
		
			printf("%d  ",x);
			x--;
		}
		printf("\n");
	} 
}
