/*
		
   	D	C	B	A
	e	d	c	b
	F	E	D	C
	g	f	e	d

*/


#include<stdio.h>

void main() {

	int rows;
	printf("Enter no. of rows\n");
	scanf("%d",&rows);

	char ch1 = 'A'+rows-1;
	char ch2 = 'a'+rows-1;

	int x = 2;

	for(int i = 1; i<=rows; i++) {

		for(int j=1; j<=rows; j++) {
			
			if(i%2 == 0) {
			
				printf("%c  ",ch2);
				ch2--;
			}else{
			
				printf("%c  ",ch1);
				ch1--;
			}
		}
		ch1 = ch1+rows+1;
		ch2 = ch2+rows+1;
		x = x+2;
		printf("\n");
	} 
}
