// gets() :- Reads upto \n

#include<stdio.h>

void main() {

	char name[20];
	gets(name);		//dangerous function and should not be used

	printf("%s\n",name);
	
	puts(name);
}
