#include<stdio.h>

char* mystrncpy(char *dest, char* src, int n) {

	while(n>=1 && *src != '\0') {
	
		*dest = *src;
		dest++;
		src++;
		n--;
	}
	*dest = '\0';
}

void main() {

	char* str1 = "Dhiraj More";
	char arr[20];

	int n;
	printf("Enter value at which number to copy\n");
	scanf("%d",&n);

	mystrncpy(arr,str1,n);

	puts(str1);
	puts(arr);
}
