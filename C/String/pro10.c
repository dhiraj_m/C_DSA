#include<stdio.h>

char* mystrncpy(char *src, char* dest, int n) {

	while(*src != '\0')
		src++;

	while(n>=1 && *dest != '\0') {
	
		*src = *dest;
		dest++;
		src++;
		n--;
	}
	*src = '\0';
}

void main() {

	char str1[20] = "Dhiraj";
	char *str2 = "More";

	puts(str1);
	puts(str2);
	
	int n;
	printf("Enter value at which number to copy\n");
	scanf("%d",&n);

	mystrncpy(str1,str2,n);

	puts(str1);
	puts(str2);
}
