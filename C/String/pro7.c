#include <stdio.h>

char* mystrlwr(char* str) {

	while(*str != '\0') {
	
		if(65<=*str && *str<=90) 
			*str = *str+32;
		str++;
	}
}

void main() {

	char arr[] = {'D','h','I','r','A','J'};
	puts(arr);

	mystrlwr(arr);

	puts(arr);
}
