#include<stdio.h>

char* strrev(char* str) {

	char* temp = str;
	while(*temp != '\0')
		temp++;

	temp--;

	char x;
	while(str<temp) {
	
		x = *str;
		*str = *temp;
		*temp = x;
		temp--;
		str++;
	}
}

void main() {

	char arr[10] = "Dhiraj";

	puts(arr);

	strrev(arr);

	puts(arr);
}
